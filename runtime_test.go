/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package evm

import (
	"encoding/binary"
	"encoding/hex"
	"encoding/pem"
	"testing"

	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	acPb "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"

	"chainmaker.org/chainmaker/common/v2/evmutils"
	"chainmaker.org/chainmaker/protocol/v2"
	"github.com/stretchr/testify/require"

	"chainmaker.org/chainmaker/logger/v2"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/vm-evm/v2/test"
)

const (
	chainId              = "chain01"
	certFilePath         = "./test/config/admin1.sing.crt"
	tokenContractBinFile = "./test/contracts/contract01/token.bin"
	storeContractBinFile = "./test/contracts/contract02/storage.bin"
	initMethod           = "init_contract"
	contractTokenName    = "contract_token"
	contractStoreName    = "contract_store"
	storeBodyFile        = "./test/contracts/contract02/storage_body.bin"
	tokenBodyFile        = "./test/contracts/contract01/token_body.bin"
)

func TestInstalContractToken(t *testing.T) {
	//部署合约
	method := initMethod
	test.CertFilePath = certFilePath
	test.ContractName = contractTokenName
	test.ByteCodeFile = tokenContractBinFile
	parameters := make(map[string][]byte)
	contractId, txContext, byteCode := test.InitContextTest(common.RuntimeType_EVM)

	runtimeInstance := &RuntimeInstance{
		ChainId:      chainId,
		Log:          logger.GetLogger(logger.MODULE_VM),
		TxSimContext: txContext,
	}

	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)

	byteCode, _ = hex.DecodeString(string(byteCode))
	test.BaseParam(parameters)
	parameters["data"] = []byte("00000000000000000000000013f0c1639a9931b0ce17e14c83f96d4732865b58")
	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("ContractResult Result:%+X", contractResult.Result)
}

func TestInvokeToken(t *testing.T) {
	//调用合约
	method := "4f9d719e" //method testEvent
	test.ContractName = contractTokenName
	test.ByteCodeFile = tokenBodyFile
	test.CertFilePath = certFilePath
	parameters := make(map[string][]byte)
	contractId, txContext, byteCode := test.InitContextTest(common.RuntimeType_EVM)

	runtimeInstance := &RuntimeInstance{
		ChainId:      chainId,
		Log:          logger.GetLogger(logger.MODULE_VM),
		TxSimContext: txContext,
	}

	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)

	byteCode, _ = hex.DecodeString(string(byteCode))
	test.BaseParam(parameters)
	parameters["data"] = []byte("4f9d719e")

	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("method testEvent-- ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("method testEvent-- ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("method testEvent-- ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("method testEvent-- ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("method testEvent-- ContractResult Result:%+X", contractResult.Result)
}

func TestInstalContractStorage(t *testing.T) {
	//部署合约
	method := initMethod
	test.ContractName = contractStoreName
	test.CertFilePath = certFilePath
	test.ByteCodeFile = storeContractBinFile
	parameters := make(map[string][]byte)
	contractId, txContext, byteCode := test.InitContextTest(common.RuntimeType_EVM)

	runtimeInstance := &RuntimeInstance{
		ChainId:      chainId,
		Log:          logger.GetLogger(logger.MODULE_VM),
		TxSimContext: txContext,
	}

	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)

	byteCode, _ = hex.DecodeString(string(byteCode))
	test.BaseParam(parameters)
	//parameters["data"] = []byte("00000000000000000000000013f0c1639a9931b0ce17e14c83f96d4732865b58")
	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("ContractResult Result:%+X", contractResult.Result)
}

func TestInvokeStorage(t *testing.T) {
	//调用合约
	method := "6057361d" //method store
	test.ContractName = contractStoreName
	test.ByteCodeFile = storeBodyFile
	test.CertFilePath = certFilePath
	parameters := make(map[string][]byte)
	contractId, txContext, byteCode := test.InitContextTest(common.RuntimeType_EVM)

	runtimeInstance := &RuntimeInstance{
		ChainId:      chainId,
		Log:          logger.GetLogger(logger.MODULE_VM),
		TxSimContext: txContext,
	}

	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)

	byteCode, _ = hex.DecodeString(string(byteCode))
	test.BaseParam(parameters)
	parameters["data"] = []byte("6057361d0000000000000000000000000000000000000000000000000000000000000064")

	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("method store-- ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("method store-- ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("method store-- ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("method store-- ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("method store-- ContractResult Result:%+X", contractResult.Result)

	test.BaseParam(parameters)
	method = "2e64cec1" //method retrieve
	parameters["data"] = []byte("2e64cec1")
	contractResult, _ = runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("method retrieve-- ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("method retrieve-- ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("method retrieve-- ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("method retrieve-- ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("method retrieve-- ContractResult Result:%+X", contractResult.Result)

}

//func TestConvertEvmContractName(t *testing.T) {
//	name := "0x7162629f540a9e19eCBeEa163eB8e48eC898Ad00"
//	addr, _ := contractNameToAddress(name)
//	t.Logf("evm addr:%s", addr.Text(16))
//	assert.Equal(t, strings.ToLower(name[2:]), addr.Text(16))
//}

func TestAddrFromBaseParams(t *testing.T) {
	typeParam := make([]byte, 4)
	binary.BigEndian.PutUint32(typeParam, uint32(1))

	pkType := make([]byte, 4)
	binary.BigEndian.PutUint32(pkType, uint32(acPb.MemberType_CERT))

	certType := make([]byte, 4)
	binary.BigEndian.PutUint32(certType, uint32(acPb.MemberType_PUBLIC_KEY))

	creatorCertStr := "-----BEGIN CERTIFICATE-----\nMIICijCCAi+gAwIBAgIDBS9vMAoGCCqGSM49BAMCMIGKMQswCQYDVQQGEwJDTjEQ\nMA4GA1UECBMHQmVpamluZzEQMA4GA1UEBxMHQmVpamluZzEfMB0GA1UEChMWd3gt\nb3JnMS5jaGFpbm1ha2VyLm9yZzESMBAGA1UECxMJcm9vdC1jZXJ0MSIwIAYDVQQD\nExljYS53eC1vcmcxLmNoYWlubWFrZXIub3JnMB4XDTIwMTIwODA2NTM0M1oXDTI1\nMTIwNzA2NTM0M1owgZExCzAJBgNVBAYTAkNOMRAwDgYDVQQIEwdCZWlqaW5nMRAw\nDgYDVQQHEwdCZWlqaW5nMR8wHQYDVQQKExZ3eC1vcmcxLmNoYWlubWFrZXIub3Jn\nMQ8wDQYDVQQLEwZjbGllbnQxLDAqBgNVBAMTI2NsaWVudDEuc2lnbi53eC1vcmcx\nLmNoYWlubWFrZXIub3JnMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE56xayRx0\n/a8KEXPxRfiSzYgJ/sE4tVeI/ZbjpiUX9m0TCJX7W/VHdm6WeJLOdCDuLLNvjGTy\nt8LLyqyubJI5AKN7MHkwDgYDVR0PAQH/BAQDAgGmMA8GA1UdJQQIMAYGBFUdJQAw\nKQYDVR0OBCIEIMjAiM2eMzlQ9HzV9ePW69rfUiRZVT2pDBOMqM4WVJSAMCsGA1Ud\nIwQkMCKAIDUkP3EcubfENS6TH3DFczH5dAnC2eD73+wcUF/bEIlnMAoGCCqGSM49\nBAMCA0kAMEYCIQCWUHL0xisjQoW+o6VV12pBXIRJgdeUeAu2EIjptSg2GAIhAIxK\nLXpHIBFxIkmWlxUaanCojPSZhzEbd+8LRrmhEO8n\n-----END CERTIFICATE-----\n"
	creatorBlock, _ := pem.Decode([]byte(creatorCertStr))
	creatorCert, _ := bcx509.ParseCertificate(creatorBlock.Bytes)
	creatorPk, _ := creatorCert.PublicKey.String()
	creatorAddrStr := "ZX8fee833329840b773103ce26bc557784ba667ee7"

	senderCertStr := "-----BEGIN CERTIFICATE-----\nMIICiDCCAi6gAwIBAgIDBsRmMAoGCCqGSM49BAMCMIGKMQswCQYDVQQGEwJDTjEQ\nMA4GA1UECBMHQmVpamluZzEQMA4GA1UEBxMHQmVpamluZzEfMB0GA1UEChMWd3gt\nb3JnMi5jaGFpbm1ha2VyLm9yZzESMBAGA1UECxMJcm9vdC1jZXJ0MSIwIAYDVQQD\nExljYS53eC1vcmcyLmNoYWlubWFrZXIub3JnMB4XDTIwMTIwODA2NTM0M1oXDTI1\nMTIwNzA2NTM0M1owgZAxCzAJBgNVBAYTAkNOMRAwDgYDVQQIEwdCZWlqaW5nMRAw\nDgYDVQQHEwdCZWlqaW5nMR8wHQYDVQQKExZ3eC1vcmcyLmNoYWlubWFrZXIub3Jn\nMQ8wDQYDVQQLEwZjbGllbnQxKzApBgNVBAMTImNsaWVudDEudGxzLnd4LW9yZzIu\nY2hhaW5tYWtlci5vcmcwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATKeQy3pk5q\n0VKCotkU2NWrCe7zPXFVN+c6tqu0ZN4YJEUrly0Y2APa+FxIkLCtdY6Zm76FcMPI\nNLREJsNxtK9Uo3sweTAOBgNVHQ8BAf8EBAMCAaYwDwYDVR0lBAgwBgYEVR0lADAp\nBgNVHQ4EIgQg+gKwntmyXoywcVZmjK9AUcpgWYhHKgRm5+Qgu0Ce/rEwKwYDVR0j\nBCQwIoAg8Y/Vs9Pj8uezY+di51n3+oexybSkYvop/L7UIAVYbSEwCgYIKoZIzj0E\nAwIDSAAwRQIgEWNAlLNYp8+QwkLsu3inoPlvDjSGGV/NPVxF9NZ38acCIQCnVUG6\n+SapOFAKTAcrBXIUkQcieTYTxF5C65lO/2MXWw==\n-----END CERTIFICATE-----"
	senderBlock, _ := pem.Decode([]byte(senderCertStr))
	senderCert, _ := bcx509.ParseCertificate(senderBlock.Bytes)
	senderPk, _ := senderCert.PublicKey.String()
	senderAddrStr := "ZX13466813949966647197660e4bd0d3459fc3d2ce"

	tests := []struct {
		name   string
		params map[string][]byte
		result map[string]*evmutils.Int
		expErr error
	}{
		{
			"ZXL_ADDR",
			map[string][]byte{
				protocol.ContractAddrTypeParam:    typeParam,
				protocol.ContractCreatorPkParam:   []byte(creatorCertStr),
				protocol.ContractSenderPkParam:    []byte(senderCertStr),
				protocol.ContractCreatorTypeParam: pkType,
				protocol.ContractSenderTypeParam:  pkType,
			},
			map[string]*evmutils.Int{
				"creatorAddr": evmutils.FromHexString(creatorAddrStr[2:]),
				"senderAddr":  evmutils.FromHexString(senderAddrStr[2:]),
			},
			nil,
		},
		{
			"ZXL_ADDR",
			map[string][]byte{
				protocol.ContractAddrTypeParam:    typeParam,
				protocol.ContractCreatorPkParam:   []byte(creatorPk),
				protocol.ContractSenderPkParam:    []byte(senderPk),
				protocol.ContractCreatorTypeParam: certType,
				protocol.ContractSenderTypeParam:  certType,
			},
			map[string]*evmutils.Int{
				"creatorAddr": evmutils.FromHexString(creatorAddrStr[2:]),
				"senderAddr":  evmutils.FromHexString(senderAddrStr[2:]),
			},
			nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cAddr, sAddr, _, err := AddrFromBaseParams(tt.params)
			require.Nil(t, err)

			require.Equal(t, tt.expErr, err)
			require.Equal(t, tt.result["creatorAddr"], cAddr)
			require.Equal(t, tt.result["senderAddr"], sAddr)
		})
	}
}
