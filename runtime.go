/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package evm

import (
	"encoding/binary"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"runtime/debug"

	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	acPb "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"

	"chainmaker.org/chainmaker/common/v2/crypto"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/common/v2/evmutils"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/protocol/v2"
	evmGo "chainmaker.org/chainmaker/vm-evm/v2/evm-go"
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/environment"
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/opcodes"
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/storage"
)

// RuntimeInstance evm runtime
type RuntimeInstance struct {
	Method        string             // invoke contract method
	ChainId       string             // chain id
	Address       *evmutils.Int      //address
	Contract      *commonPb.Contract // contract info
	Log           protocol.Logger
	TxSimContext  protocol.TxSimContext
	ContractEvent []*commonPb.ContractEvent
}

type InstancesManager struct {
}

func (*InstancesManager) NewRuntimeInstance(txSimContext protocol.TxSimContext, chainId, method string, codePath string,
	contract *commonPb.Contract, byteCode []byte, log protocol.Logger) (protocol.RuntimeInstance, error) {
	return &RuntimeInstance{
		Method:       method,
		ChainId:      chainId,
		Contract:     contract,
		TxSimContext: txSimContext,
		Log:          log,
	}, nil
}

func AddrFromBaseParams(parameters map[string][]byte) (creatorAddr, senderAddr *evmutils.Int, addrType int32,
	e error) {
	addrType = int32(binary.BigEndian.Uint32(parameters[protocol.ContractAddrTypeParam]))
	if addrType == int32(config.AddrType_ZXL) {
		var (
			pk crypto.PublicKey
			er error
		)

		creatorType := int32(binary.BigEndian.Uint32(parameters[protocol.ContractCreatorTypeParam]))
		if creatorType == int32(acPb.MemberType_PUBLIC_KEY) {
			pk, er = asym.PublicKeyFromPEM(parameters[protocol.ContractCreatorPkParam])
			if er != nil {
				return nil, nil, addrType, errors.New("failed to parse public key from creator")
			}
		} else {
			createBlock, _ := pem.Decode(parameters[protocol.ContractCreatorPkParam])
			createCert, err := bcx509.ParseCertificate(createBlock.Bytes)
			if err != nil {
				return nil, nil, addrType, errors.New("failed to parse cert from creator")
			}
			pk = createCert.PublicKey
		}
		creatorAddrStr, errc := evmutils.ZXAddressFromPublicKey(pk)
		if errc != nil {
			return nil, nil, addrType, errors.New("calculate zxl address for creator pk fail")
		}
		creatorAddr = evmutils.FromHexString(creatorAddrStr[2:])
		parameters[protocol.ContractCreatorPkParam], _ = pk.Bytes()

		senderType := int32(binary.BigEndian.Uint32(parameters[protocol.ContractSenderTypeParam]))
		if senderType == int32(acPb.MemberType_PUBLIC_KEY) {
			pk, er = asym.PublicKeyFromPEM(parameters[protocol.ContractSenderPkParam])
			if er != nil {
				return nil, nil, addrType, errors.New("failed to parse public key from creator")
			}
		} else {
			senderBlock, _ := pem.Decode(parameters[protocol.ContractSenderPkParam])
			senderCert, err := bcx509.ParseCertificate(senderBlock.Bytes)
			if err != nil {
				return creatorAddr, nil, addrType, errors.New("failed to parse cert from sender")
			}
			pk = senderCert.PublicKey
		}
		senderAddrStr, errs := evmutils.ZXAddressFromPublicKey(pk)
		if errs != nil {
			return creatorAddr, nil, addrType, errors.New("calculate zxl address for sender pk fail")
		}
		senderAddr = evmutils.FromHexString(senderAddrStr[2:])
		parameters[protocol.ContractSenderPkParam], _ = pk.Bytes()
	} else {
		var err error
		creatorAddr, err = evmutils.MakeAddressFromHex(string(parameters[protocol.ContractCreatorPkParam]))
		if err != nil {
			return nil, nil, addrType, errors.New("get creator pk fail")
		}

		senderAddr, err = evmutils.MakeAddressFromHex(string(parameters[protocol.ContractSenderPkParam]))
		if err != nil {
			return creatorAddr, nil, addrType, errors.New("get sender pk fail")
		}
	}

	delete(parameters, protocol.ContractAddrTypeParam)
	return creatorAddr, senderAddr, addrType, nil
}

// init just load instructions once
func init() {
	// init memory and env
	evmGo.Load()
	// execute method
}

// Invoke contract by call vm, implement protocol.RuntimeInstance
func (r *RuntimeInstance) Invoke(contract *commonPb.Contract, method string, byteCode []byte,
	parameters map[string][]byte, txSimContext protocol.TxSimContext, gasUsed uint64) (
	contractResult *commonPb.ContractResult, specialTxType protocol.ExecOrderTxType) {
	txId := txSimContext.GetTx().Payload.TxId
	r.Log.Debugf("evm runtime start to run contract, tx id:%s", txId)
	// contract response
	contractResult = &commonPb.ContractResult{
		Code:    uint32(1),
		Result:  nil,
		Message: "",
	}
	specialTxType = protocol.ExecOrderTxTypeNormal

	defer func() {
		if err := recover(); err != nil {
			r.Log.Errorf("failed to invoke evm, tx id:%s, error:%s", txId, err)
			contractResult.Code = 1
			if e, ok := err.(error); ok {
				contractResult.Message = e.Error()
			} else if e, ok := err.(string); ok {
				contractResult.Message = e
			}
			debug.PrintStack()
		}
	}()

	// merge evm param
	//todo sdk常量
	r.Log.Debugf("evm runtime begin to process params, tx id:%s", txId)
	params := string(parameters[protocol.ContractEvmParamKey])
	isDeploy := false
	if method == protocol.ContractInitMethod || method == protocol.ContractUpgradeMethod {
		isDeploy = true
	} else {
		if evmutils.Has0xPrefix(method) {
			method = method[2:]
		}
		if len(method) != 8 {
			return r.errorResult(contractResult, nil, "contract verify failed, method length is not 8")
		}
	}
	if evmutils.Has0xPrefix(params) {
		params = params[2:]
	}
	if len(params)%2 == 1 {
		params = "0" + params
	}

	messageData, err := hex.DecodeString(params)
	if err != nil {
		return r.errorResult(contractResult, err, "params is not hex encode string")
	}
	if isDeploy {
		messageData = append(byteCode, messageData...)
		byteCode = messageData
	}
	r.Log.Debugf("evm runtime begin to get creator, sender and contract addresses, tx id:%s", txId)
	// evmTransaction
	creatorAddress, senderAddress, addrType, err := AddrFromBaseParams(parameters)
	if err != nil {
		return r.errorResult(contractResult, err, err.Error())
	}

	gasLeft := protocol.GasLimit - gasUsed
	evmTransaction := environment.Transaction{
		TxHash:   []byte(txId),
		Origin:   senderAddress,
		GasPrice: evmutils.New(protocol.EvmGasPrice),
		GasLimit: evmutils.New(int64(gasLeft)),
	}

	// contract
	//address, err := evmutils.MakeAddressFromString(contract.Name) // reference vm_factory.go RunContract
	address, err := contractNameHexToAddress(contract.Name)
	if err != nil {
		return r.errorResult(contractResult, err, "make address fail")
	}
	r.Log.Debugf("evm runtime begin to get code hash, tx id:%s", txId)
	codeHash := evmutils.BytesDataToEVMIntHash(byteCode)
	eContract := environment.Contract{
		Address: address,
		Code:    byteCode,
		Hash:    codeHash,
	}
	r.Address = address
	r.Contract = contract
	// new evm instance
	r.Log.Debugf("evm runtime begin to new evm instance, tx id:%s", txId)
	//lastBlock, _ := txSimContext.GetBlockchainStore().GetLastBlock()
	blockTimestamp := txSimContext.GetBlockTimestamp()
	blockHeight := txSimContext.GetBlockHeight()
	r.Log.Debugf("evm runtime get last block timestamp:%v, height:%d, tx id:%s",
		blockTimestamp, blockHeight, txId)
	externalStore := &storage.ContractStorage{Ctx: txSimContext}
	evm := evmGo.New(evmGo.EVMParam{
		MaxStackDepth:  protocol.EvmMaxStackDepth,
		ExternalStore:  externalStore,
		ResultCallback: r.callback,
		Context: &environment.Context{
			Block: environment.Block{
				Coinbase:   creatorAddress, //proposer ski
				Timestamp:  evmutils.New(blockTimestamp),
				Number:     evmutils.New(int64(blockHeight)), // height
				Difficulty: evmutils.New(0),
				GasLimit:   evmutils.New(protocol.GasLimit),
			},
			Contract:    eContract,
			Transaction: evmTransaction,
			Message: environment.Message{
				Caller: senderAddress,
				Value:  evmutils.New(0),
				Data:   messageData,
			},
			Parameters: parameters,
			Cfg: environment.Config{
				AddrType: addrType,
			},
		},
	})
	// init memory and env
	//evmGo.Load()
	// execute method
	r.Log.Debugf("evm runtime start to execute contract, tx id:%s, isDeploy:%v", txId, isDeploy)
	result, err := evm.ExecuteContract(isDeploy)
	pcCount, timeUsed := evm.GetPcCountAndTimeUsed()
	if err != nil {
		r.Log.Errorf("evm runtime execute contract failed, tx id:%s, pc count:%v, time used:%v, error:%v",
			txId, pcCount, timeUsed, err)
		return r.errorResult(contractResult, err, "failed to execute evm contract")
	}
	r.Log.Debugf("evm runtime execute contract finished, tx id:%s, pc count:%v, time used:%v, isDeploy:%v",
		txId, pcCount, timeUsed, isDeploy)
	contractResult.Code = 0
	contractResult.GasUsed = gasLeft - result.GasLeft
	contractResult.Result = result.ResultData
	contractResult.ContractEvent = r.ContractEvent
	return contractResult, protocol.ExecOrderTxTypeNormal
}

//func contractNameDecimalToAddress(cname string) (*evmutils.Int, error) {
//	// hexStr2 == hexStr2
//	// hexStr := hex.EncodeToString(evmutils.Keccak256([]byte("contractName")))[24:]
//	// hexStr2 := hex.EncodeToString(evmutils.Keccak256([]byte("contractName"))[12:])
//	// 为什么使用十进制字符串转换，因为在./evm-go中，使用的是 address.String()作为key，也就是说数据库的名称是十进制字符串。
//	evmAddr := evmutils.FromDecimalString(cname)
//	if evmAddr == nil {
//		return nil, errors.New("contractName[%s] not DecimalString,
//		you can use evmutils.MakeAddressFromString(\"contractName\").String() get a decimal string")
//	}
//	return evmAddr, nil
//}
func contractNameHexToAddress(cname string) (*evmutils.Int, error) {
	evmAddr := evmutils.FromHexString(cname)
	if evmAddr == nil {
		return nil, errors.New("contractName[%s] not HexString, you can use hex.EncodeToString(" +
			"evmutils.MakeAddressFromString(\"contractName\").Bytes()) get a hex string address")
	}
	return evmAddr, nil
}
func (r *RuntimeInstance) callback(result evmGo.ExecuteResult, err error) {
	if result.ExitOpCode == opcodes.REVERT {
		err = fmt.Errorf("revert instruction was encountered during execution")
		r.Log.Errorf("revert instruction encountered in contract [%s] execution，tx: [%s], error: [%s]",
			r.Contract.Name, r.TxSimContext.GetTx().Payload.TxId, err.Error())
		panic(err)
	}
	if err != nil {
		r.Log.Errorf("error encountered in contract [%s] execution，tx: [%s], error: [%s]",
			r.Contract.Name, r.TxSimContext.GetTx().Payload.TxId, err.Error())
		panic(err)
	}
	//emit  contract event
	err = r.emitContractEvent(result)
	if err != nil {
		r.Log.Errorf("emit contract event err:%s", err.Error())
		panic(err)
	}
	for n, v := range result.StorageCache.CachedData {
		for k, val := range v {
			err := r.TxSimContext.Put(n, []byte(k), val.Bytes())
			if err != nil {
				r.Log.Errorf("callback txSimContext put err:%s", err.Error())
			}
			//fmt.Println("n k val", n, k, val, val.String())
		}
	}
	r.Log.Debug("result:", result.ResultData)
}

func (r *RuntimeInstance) errorResult(contractResult *commonPb.ContractResult, err error, errMsg string) (
	*commonPb.ContractResult, protocol.ExecOrderTxType) {
	contractResult.Code = 1
	if err != nil {
		errMsg += ", " + err.Error()
	}
	contractResult.Message = errMsg
	r.Log.Error(errMsg)
	return contractResult, protocol.ExecOrderTxTypeNormal
}
func (r *RuntimeInstance) emitContractEvent(result evmGo.ExecuteResult) error {
	//parse log
	var contractEvents []*commonPb.ContractEvent
	logsMap := result.StorageCache.Logs
	for _, logs := range logsMap {
		for _, log := range logs {
			if len(log.Topics) > protocol.EventDataMaxCount-1 {
				return fmt.Errorf("too many event data")
			}
			contractEvent := &commonPb.ContractEvent{
				TxId:            r.TxSimContext.GetTx().Payload.TxId,
				ContractName:    r.Contract.Name,
				ContractVersion: r.Contract.Version,
			}
			topics := log.Topics
			for index, topic := range topics {
				//the first topic in log as contract event topic,others as event data.
				//in ChainMaker contract event,only has one topic filed.
				if index == 0 && topic != nil {
					topicHexStr := hex.EncodeToString(topic)
					if err := protocol.CheckTopicStr(topicHexStr); err != nil {
						return fmt.Errorf(err.Error())
					}
					contractEvent.Topic = topicHexStr
					r.Log.Debugf("topicHexString: %s", topicHexStr)
					continue
				}
				//topic marked by 'index' in ethereum as contract event data
				topicIndexHexStr := hex.EncodeToString(topic)
				r.Log.Debugf("topicIndexString: %s", topicIndexHexStr)
				contractEvent.EventData = append(contractEvent.EventData, topicIndexHexStr)
			}
			data := log.Data
			dataHexStr := hex.EncodeToString(data)
			if len(dataHexStr) > protocol.EventDataMaxLen {
				return fmt.Errorf("event data too long,longer than %v", protocol.EventDataMaxLen)
			}
			contractEvent.EventData = append(contractEvent.EventData, dataHexStr)
			contractEvents = append(contractEvents, contractEvent)
			r.Log.Debugf("dataHexStr: %s", dataHexStr)
		}
	}
	r.ContractEvent = contractEvents
	return nil
}

func (*InstancesManager) StartVM() error {
	return nil
}

func (*InstancesManager) StopVM() error {
	return nil
}
